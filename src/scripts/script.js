var pupil = document.getElementById('book-detail__pupil');
document.addEventListener('mousemove', function (event){
  var x = Math.max(-50,Math.min(75*((event.clientX - pupil.getBoundingClientRect().left-15) / (document.documentElement.clientWidth - pupil.getBoundingClientRect().left)),50));
  var y = Math.max(-50,Math.min(75*((event.clientY - pupil.getBoundingClientRect().top-15) / (document.documentElement.clientHeight - pupil.getBoundingClientRect().top)),50));
  pupil.style.transform = 'translateX(' + x + 'px) translateY(' + y + 'px)';
} )