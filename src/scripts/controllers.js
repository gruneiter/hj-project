fbbApp.controller('FfbMainCtrl', function($scope, $http, $sce) {
  $scope.$sce = $sce;
  $scope.pageShow = {
    mainPageShow:true,
    bookPageShow:false,
    aboutPageShow:false,
    orderPageShow: false
  }
  $scope.mainPageShow = function () {
    if ($scope.pageShow.mainPageShow == false) {
      allPagesHide ();
      $scope.pageShow.mainPageShow = true;
    }
  }
  $scope.aboutPageShow = function () {
    if ($scope.pageShow.aboutPageShow == false) {
      allPagesHide ();
      $scope.pageShow.aboutPageShow = true;
    }
  }
  $scope.bookPageShow = function (id) {
    allPagesHide ();
    $scope.pageShow.bookPageShow = true;
    $http.get('https://netology-fbb-store-api.herokuapp.com/book/' + id).success(function(data) {
      $scope.bookCurrent = data;
      $scope.totalPrice = $scope.bookCurrent.price;
    });
  }
  function allPagesHide () {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    for ( var item in $scope.pageShow ) {
      $scope.pageShow[item] = false;
    }
    $scope.orderResult = false;
    $scope.bookCurrent = {cover:{large:' '}};
  }
  $http.get('https://netology-fbb-store-api.herokuapp.com/book').success(function(data) {
    $scope.books = data;
    $scope.booksMoreButtonShow = true;
    $scope.quantity = 4;
    $scope.bookListLoadMore = function () {
      $scope.quantity+=4;
      $scope.booksMoreButtonShow = $scope.quantity < $scope.books.length;
    }
  });
  $scope.orderPage = function () {
    document.documentElement.scrollTop = 0;
    document.body.scrollTop = 0;
    $scope.pageShow.orderPageShow = !$scope.pageShow.orderPageShow;
  }
  $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery').success(function(data) {
    document.documentElement.scrollTop = 0;
    document.body.scrollTop = 0;
    $scope.deliveryMethods = data;
  });
  $scope.deliverySwitch = function (method) {
    $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery/' + method.id + '/payment').success(function(data) {
      $scope.paymentMethods = data;
    });
    $scope.deliveryPrice = method.price;
    $scope.totalPrice = $scope.bookCurrent.price + $scope.deliveryPrice
    $scope.showAddress = method.needAdress;
  }
  $scope.order = {};
  $scope.orderResult = false;
  $scope.addOrder = function(order) {
    $scope.order = order;
    $scope.order.book = $scope.bookCurrent.id;
    $scope.order.payment.currency = $scope.bookCurrent.currency;
    $scope.order.manager = 'gruneiter@gmail.com';
    $http({
      url: 'https://netology-fbb-store-api.herokuapp.com/order',
      method: "POST",
      data: $scope.order,
      headers: {'Content-Type': 'application/json'}
    }).success(function () {
      document.documentElement.scrollTop = 0;
      document.body.scrollTop = 0;
      $scope.orderResult = true;
      $scope.orderResultSuccess = true;
    }).error(function () {
      document.documentElement.scrollTop = 0;
      document.body.scrollTop = 0;
      $scope.orderResult = true;
      $scope.orderResultError = true;
    });
  };
});
