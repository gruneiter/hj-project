import gulp from 'gulp';
import stylus from 'gulp-stylus';
import pug from 'gulp-pug';
import sourcemaps from 'gulp-sourcemaps';
import browserSync from 'browser-sync';
import watch from 'gulp-watch';
import gcmq from 'gulp-group-css-media-queries';
import changed from 'gulp-changed';
import autoprefixer from 'autoprefixer-stylus';
import importIfExist from 'stylus-import-if-exist';
import plumber from 'gulp-plumber';
import eslint from 'gulp-eslint';
var reload = browserSync.reload;

gulp.task('stylus', () => {
	gulp.src('./src/styles/style.styl')
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(stylus({
			use: [
				importIfExist(),
				autoprefixer()
			],
			'include css': true
		}))
		.pipe(gcmq())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./dist/styles'))
		.pipe(reload({stream:true}));
});


gulp.task('pug', () => {
	gulp.src('./src/pages/*.pug')
		.pipe(plumber())
		.pipe(pug( {
			basedir: 'src',
			pretty: true
		} ))
		.pipe(gulp.dest('./dist/'))
		.pipe(reload({stream:true}));
});
gulp.task('scripts', () => {
	gulp.src('./src/scripts/*.js')
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(gulp.dest('./dist/scripts/'))
}) 
gulp.task('copy', () => {
	gulp.src('src/resources/**/*')
		.pipe(changed('dist'))
		.pipe(gulp.dest('dist'))
		.pipe(reload({stream:true}));
});

gulp.task('watcher', () => {
	watch('./src/styles/style.styl', () => {gulp.start('stylus')});
	watch('./src/blocks/**/*.styl', () => {gulp.start('stylus')})
	watch('./src/pages/*.pug', () => {gulp.start('pug')})
	watch('./src/blocks/**/*.pug', () => {gulp.start('pug')})
	watch('./src/scripts/*.js', () => {gulp.start('scripts')})
	watch('./src/resources/', () => {gulp.start('copy')})
});

gulp.task('browserSync', ()  => {
	browserSync({
		server: {
			baseDir: [
				"dist"
			]
		},
		open: false,
		notify: false
	});
});

gulp.task('default', ['stylus', 'pug', 'copy', 'watcher', 'browserSync']);